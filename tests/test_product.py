import pytest

# List of testing barcodes


testing_codes1 = [
    "8718096160714", "5450168511156", "7612100018477", "87157321", "4008258154236",
    "5410046000141", "5060108450546", "4018077006425", "20697075", "5450148000014",
    "27043189", "8002974025708", "4002309012933",
    "40081427", "5410228253556", "3563490016908", "5901044003581", "5904378643270", "8410076801210"
]

testing_codes2 = [
    "8718096160714", "5450168511156", "7612100018477"]

testing_codes = testing_codes2

# List of failed testing barcodes
failed_testing_barcodes = ["23287235", "3297362470009", "5904194002107", "5907544131441"]


#TEST POST a Product
def test_create_product(admin_client):
    product_body = {
                    "code": "7612100018477",
                    "product_name": "test_name",
                    "image_front_url": "test_url.com"
                    }
    
    res = admin_client.post("/products/", json=product_body)
    print(res.json())
    assert res.status_code == 201

#TEST PUT a Product
def test_update_product(admin_client):
    product_body = {
                    "code": "7612100018477",
                    "product_name": "test_new_name",
                    "image_front_url": "test_new_url.com"
                    }
    
    res = admin_client.post("/products/", json=product_body)
    print(res.json())
    assert res.status_code == 201

# TEST GET ALL Products
def test_getAllProducts(client, create_product):
    res = client.get(f"/products/{testing_codes}")
    print(res.json())
    assert res.status_code == 200

# TEST GET Product BY code   
@pytest.mark.parametrize("testing_codes", testing_codes)
def test_getProductByCode(client, testing_codes):
    res = client.get(f"/products/{testing_codes}")
    assert res.status_code == 200
    assert res.json().get("code") == testing_codes
    
@pytest.mark.parametrize("testing_codes", testing_codes)
def test_getProductMaterialElement(client, testing_codes):
    res = client.get(f"/products/{testing_codes}")
    assert len(res.json().get("package_materials")) > 0
    assert len(res.json().get("package_materials")) == len(res.json().get("package_elements"))

#TEST DELETE a Product
def test_delete_product(admin_client, create_product):
    res = admin_client.delete("/products/7612100018477")
    assert res.status_code == 204




#@pytest.mark.parametrize("testing_codes", testing_codes)
#def test_package_materials_related(client, testing_codes, seed_materials):
#    res = client.get(f"/products/{testing_codes}")
#
#    # Extract package_materials and related_materials
#    package_materials = res.json().get("package_materials", [])
#    related_materials = res.json().get("related_materials", [])
#
#    # Extract names of materials from related_materials
#    related_material_names = [material['name'] for material in related_materials]
#
#    assert package_materials == related_material_names
    

    
