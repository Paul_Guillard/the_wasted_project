from jose import jwt
# from API.v1.utils.user_management import jwt_manager
import pytest

# test the creation of a user
# eg: is the status code returned 201
def test_create_user(client):
    res = client.post("/auth/register", json = {
                                                "first_name": "Grzegorz",
                                                "last_name": "Brzeczyszczykiewicz",
                                                "commune": "Luxembourg",
                                                "email": "greg@example.com",
                                                "password": "pass"
                                                } 
                      )
    assert res.status_code == 201
    print(res.json())
    #assert res.json().get("email") == "greg@something.lu"

# TEST login user    
def test_login_user(client, create_user):
    res = client.post("/auth/login", data={"username": create_user["email"],
                                     "password": create_user["password"]})
    assert res.status_code == 200
    assert res.json().get("token_type") == "bearer"
    
    # Verify Token Data
#    payload = jwt.decode(res.json().get("access_token"),
#                         jwt_manager.SERVER_KEY,
#                         [jwt_manager.ALGORITHM])
#    assert payload["user_id"] == create_user["id"]
# 
## TEST GET USER BY ID   
#def test_getUserById(authorized_client, create_user):
#    res = authorized_client.get(f"/users/{create_user['id']}")
#    assert res.status_code == 200
#    assert res.json().get("id") == create_user["id"]
#    assert res.json().get("email") == create_user["email"]
#    
#@pytest.mark.parametrize("email, password, status_code", [
#    (None, '4321', 422),
#    ('user@something.lu', None, 422),
#    ('wrongemail@gmail.com','4321', 401),
#    ('user@something.lu','wrongpassword', 401),
#    ('wrongemail@gmail.com', 'wrongpassword', 401),
#    ('user@something.lu','4321',200)
#])
#def test_credentials(client, create_user, email, password, status_code):
#    res = client.post("/auth", data={"username": email,
#                                     "password": password})
#    assert res.status_code == status_code