import pytest


def test_create_material(admin_client, create_container):
    material_body = {
                    "raw_name": "RAAaaaawwww",
                    "name": "polite raw",
                    "container_id": 1,
                    "instructions": "Some detailed instruction"
                    }
    res = admin_client.post("/materials/", json = material_body)
    print(len(res.json()))
    print(res.json())
    assert res.status_code == 201

def test_get_materials(client, create_material):
    res = client.get("/materials/")
    print(len(res.json()))
    print(res.json())
    
    assert res.status_code == 200
    
def test_get_material_by_ID(client, create_material):
    res = client.get("/materials/1")
    print(len(res.json()))
    print(res.json())
    
    assert res.status_code == 200
    assert res.json().get("name") == create_material["name"]
    assert res.json().get("raw_name") == create_material["raw_name"]
    assert res.json().get("container_id") == create_material["container_id"]
   
    
def test_update_material(admin_client, create_material):
    update_material_body = {
                            "raw_name": "MORE RAAaaaawwww",
                            "name": "MORE polite raw",
                            "container_id": 1,
                            "instructions": "Some detailed instruction"
                            }
    res = admin_client.put("/materials/1", json = update_material_body)
    print(len(res.json()))
    print(res.json())
    assert res.status_code == 200
    assert res.json().get("name") == update_material_body["name"]
    assert res.json().get("raw_name") == update_material_body["raw_name"]
    assert res.json().get("container_id") == update_material_body["container_id"]


def test_delete_material(admin_client, create_material):
    res = admin_client.delete("/materials/1")
    assert res.status_code == 204
