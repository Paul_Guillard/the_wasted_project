import pytest


def test_create_improvement(guest_client):
    improvement_body = {
                        "code": "string",
                        "product_name": "string",
                        "brand": "string",
                        "image_front_url": "string",
                        "origins": "string",
                        "element_1": "string",
                        "material_1": "string",
                        "element_2": "string",
                        "material_2": "string",
                        "element_3": "string",
                        "material_3": "string",
                        "element_4": "string",
                        "material_4": "string",
                        "element_5": "string",
                        "material_5": "string",
                        "user_token": "string"
                        }
    res = guest_client.post("/improvements/", json = improvement_body)
    print(len(res.json()))
    print(res.json())
    assert res.status_code == 201

def test_get_improvements(client, create_improvement):
    res = client.get("/improvements/")
    print(len(res.json()))
    print(res.json())
    
    assert res.status_code == 200
    
def test_get_improvement_by_ID(client, create_improvement):
    res = client.get("/improvements/1")
    print(len(res.json()))
    print(res.json())
    
    assert res.status_code == 200


def test_delete_improvement(admin_client, create_improvement):
    res = admin_client.delete("/improvements/1")
    assert res.status_code == 204