import pytest


def test_create_container(admin_client):
    container_body = {
                        "name": "name1",
                        "accepted": ["go for it"],
                        "not_accepted": ["don't you dare"],
                        "name_disp": "Display Name"
                        }
    res = admin_client.post("/containers/", json = container_body)
    print(len(res.json()))
    print(res.json())
    assert res.status_code == 201

def test_get_containers(client, create_container):
    res = client.get("/containers/")
    print(len(res.json()))
    print(res.json())
    
    assert res.status_code == 200
    
def test_get_container_by_ID(client, create_container):
    res = client.get("/containers/1")
    print(len(res.json()))
    print(res.json())
    
    assert res.status_code == 200
    assert res.json().get("name") == create_container["name"]
    assert res.json().get("accepted") == create_container["accepted"]
    assert res.json().get("not_accepted") == create_container["not_accepted"]
   
    
def test_update_container(admin_client, create_container):
    update_container_body = {
                        "name": "new_name1",
                        "accepted": ["go for it again"],
                        "not_accepted": ["I double dear you"],
                        "name_disp": "Display Name"
                        }
    res = admin_client.put("/containers/1", json = update_container_body)
    print(len(res.json()))
    print(res.json())
    assert res.status_code == 200
    assert res.json().get("name") == update_container_body["name"]
    assert res.json().get("accepted") == update_container_body["accepted"]
    assert res.json().get("not_accepted") == update_container_body["not_accepted"]

def test_delete_container(admin_client, create_container):
    res = admin_client.delete("/containers/1")
    assert res.status_code == 204