import json
import os
import pytest
from fastapi.testclient import TestClient
import requests
from API.v1.database import get_db
from API.v1.main import app
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from API.v1.config import settings
import API.v1.models as models

# Database connexion
DATABASE_URL = f"postgresql://{settings.test_database_username}:{settings.test_database_password}@{settings.test_database_host}:{settings.test_database_port}/{settings.test_database_name}"

# Running engine for ORM translation (python to SQL)
database_engine = create_engine(DATABASE_URL)

# Session template for the connection
TestSessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=database_engine)

# Dependency to create & close session on-demand
def override_db():
    db = TestSessionTemplate()
    try:
        yield db
    finally:
        db.close()


#rewuirement session creation with the database
@pytest.fixture()
def session():
    #Clean test DB by deleting the previous table
    models.Base.metadata.drop_all(bind=database_engine)
    # Create the tables
    models.Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = override_db


#requirement to run the test client (like postman)
@pytest.fixture()
def client(session):
    yield TestClient(app)


@pytest.fixture()
def guest_base_client(session):
    yield TestClient(app)


@pytest.fixture()
def admin_base_client(session):
    yield TestClient(app)


# Requirement to create a user
@pytest.fixture()
def create_guest_token(guest_base_client):
    user_credentials = {
        "first_name": "User",
        "last_name": "Test",
        "commune": "Luxembourg",
        "email": "guest@test.com",
        "password": "password00"
    }
    res = guest_base_client.post('/auth/register', json=user_credentials)
    token = res.json().get('access_token')
    return token


# Requirement to create a user
@pytest.fixture()
def create_admin_token(admin_base_client):
    user_credentials = {
        "first_name": "Admin",
        "last_name": "Test",
        "commune": "Luxembourg",
        "email": settings.admin_email,
        "password": "password00"
    }
    res = admin_base_client.post('/auth/register', json=user_credentials)
    token = res.json().get('access_token')
    return token


@pytest.fixture()
def guest_client(create_guest_token, guest_base_client):
    # ** as an argument is like the spread operator, serializes the data
    guest_base_client.headers["Authorization"] = f"bearer {create_guest_token}"
    yield guest_base_client


@pytest.fixture()
def admin_client(create_admin_token, admin_base_client):
    # ** as an argument is like the spread operator, serializes the data
    admin_base_client.headers["Authorization"] = f"bearer {create_admin_token}"
    yield admin_base_client

    
@pytest.fixture()
def create_product(admin_client):
    product_body = {
                    "code": "7612100018477",
                    "product_name": "test_name",
                    "image_front_url": "test_url.com"
                    }
    res = admin_client.post("/products/", json=product_body)
    new_product = res.json()
    return new_product


@pytest.fixture()
def seed_containers_fixture(admin_client):
    containers_req = admin_client.get("/containers")
    existing_containers = containers_req.json()

    with open("data" + os.sep + "containers.json") as containers_file:
        new_containers = json.load(containers_file)
        for container in new_containers:
            # Check if container is already there
            already_there = False
            for existing_container in existing_containers:
                if existing_container['name'] == container['name']:
                    already_there = True
            # If not there, add it to the DB
            if not already_there:
                admin_client.post('/containers', json={
                    "name": container['name'],
                    "accepted": container['accepted'],
                    "not_accepted": container['not_accepted'],
                    "name_disp": container["name_disp"]
                })


@pytest.fixture()
def seed_materials_fixture(admin_client, seed_containers_fixture):
    containers = admin_client.get("/containers")
    existing_materials_req = admin_client.get("/materials")
    existing_materials = existing_materials_req.json()

    with open("data" + os.sep + "materials.json") as materials_file:
        new_materials = json.load(materials_file)
        for material in new_materials:
            # Check if material is already there
            already_there = False
            for existing_material in existing_materials:
                if existing_material['raw_name'] == material['name']:
                    already_there = True
            # If not, add it to database
            if not already_there:
                for container in containers.json():
                    if container['name'].lower() == material['container_name'].lower():
                        container_id = container['id']
                admin_client.post('/materials', json={
                    "raw_name": material['name'],
                    "name": material['eng_name'],
                    "container_id": container_id,
                    "instructions": material["instructions"]
                })

    
# CONTAINER Fixtures
@pytest.fixture()
def create_container(admin_client):
    container_body = {
                        "name": "name1",
                        "accepted": ["go for it"],
                        "not_accepted": ["don't you dare"],
                        "name_disp": "Display Name"
                        }
    res = admin_client.post("/containers/", json = container_body)
    new_container = res.json()

    return new_container


@pytest.fixture()
def create_material(admin_client, create_container):
    material_body = {
                    "raw_name": "RAAaaaawwww",
                    "name": "polite raw",
                    "container_id": 1,
                    "instructions": "Some detailed instruction"
                    }
    res = admin_client.post("/materials/", json = material_body)
    new_material = res.json()
    return new_material

@pytest.fixture()
def create_user(client):
    post_body = {
                "first_name": "Grzegorz",
                "last_name": "Brzeczyszczykiewicz",
                "commune": "Luxembourg",
                "email": "greg@example.com",
                "password": "pass"
                }
    res = client.post("/auth/register", json = post_body)
    new_user = res.json()
    # add the password the new_user data
    new_user["password"] = post_body["password"]
    new_user["email"] = post_body["email"]
    return new_user

@pytest.fixture()
def create_improvement(guest_client):
    improvement_body = {
                        "code": "string",
                        "product_name": "string",
                        "brand": "string",
                        "image_front_url": "string",
                        "origins": "string",
                        "element_1": "string",
                        "material_1": "string",
                        "element_2": "string",
                        "material_2": "string",
                        "element_3": "string",
                        "material_3": "string",
                        "element_4": "string",
                        "material_4": "string",
                        "element_5": "string",
                        "material_5": "string",
                        "user_token": "string"
                        }
    res = guest_client.post("/improvements/", json = improvement_body)
    new_improvement = res.json()
    return new_improvement