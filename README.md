## Python Final Project 

Authors: Natalia Stepanova, Tim Zuzanski, Paul Guillard

Start Date: 22/03/2024

## Project general description

#### Title : The wasted project

The project aims at providing indications to people living in Luxembourg about how to handle waste 
and about the locality of the product. The purpose is to help them sort their waste in the most 
adapted way possible, and to guide them into consuming local products

## Git rules

Production branch: 'main'

Development branch: 'dev'

Feature branches: 'feature-###-v##' where ### is a description of the feature in 1 or 2 words, and v## is the optional version of the branch, in case the same topic needs to be worked on several times

Bug fix branches: 'bugfix-###' where ### is a short description of the bug in 2-3 words

All features and bugfixes should be merged on the dev branch. The main branch should only be used for production deployment

## Environment file

Database credentials as well as server key for the JWT token will be stored in the .env file

The .env file is not shared on git, and should be created locally by each developer. 

A template for the .env file is available publicly, named .env_base

The content of the .env file is managed by the config.py file, in the root folder, which makes all .env values available using the settings object (eg: settings.database_username)
