from pydantic_settings import BaseSettings

class Settings(BaseSettings):
    database_host: str
    database_port: str
    database_username: str
    database_password: str
    database_name: str
    test_database_host: str
    test_database_port: str
    test_database_username: str
    test_database_password: str
    test_database_name: str
    server_key: str
    algorithm: str
    expiration_minutes: int
    base_url: str
    admin_email: str
    reload_db: bool

    class Config:
        env_file = ".env"

settings = Settings()
