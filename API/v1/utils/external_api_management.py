import json
import os
import requests
from sqlalchemy.orm import Session
from ..models import Material


element_keyword_replacements = {
    'bouteille': 'Bottle',
    'flasche': 'Bottle',
    'bottle': 'Bottle',
    'corc': 'Corc',
    'pot': 'Jar',
    'jar':'Jar',
    'lid':'Lid',
    'couvercle':'Lid',
    'bocal':'Jar'
}


def list_materials(db: Session) -> dict:
    materials = db.query(Material).filter(Material.validated, True).all()
    equivalence_table = {}
    for material in materials:
        equivalence_table[material.raw_name] = material.name
    
    return equivalence_table


def create_new_material(material: str, db: Session) -> None:
    if material.find(':') > -1:
        material = material[3:]
    already_in_db = db.query(Material).filter(Material.raw_name == material).first()
    
    if not already_in_db:
        new_material = Material(
            raw_name=material,
            name="",
            container_id=None,
            instructions="",
            validated=False
        )
        db.add(new_material) #send the query
        db.commit() #save staged changes to the DB


def get_lux_communes_list():
    communes_list = []
    with open("API" + os.sep + "v1" + os.sep + "data" + os.sep + "communes.json", "r") as communes_file:
        communes = json.load(communes_file)
        for commune in communes:
            communes_list.append(commune['NOM'])
    return communes_list

# Function to fetch product data from Open Food Facts API
def get_data(barcode):
    """
    Fetches product data from the Open Food Facts API for the given barcode.

    Args:
        barcode (int): The barcode of the product.

    Returns:
        dict: A dictionary containing the product data.
    """
    try:
        resp = requests.get(url=f"https://world.openfoodfacts.org/api/v2/product/{barcode}.json")
        resp.raise_for_status()  # Raise an error for bad response status codes
        resp_body = resp.json() 
        
        product = resp_body.get('product', {})  # Check if 'product' key exists in the response
        communes = get_lux_communes_list()
        data = {}
        # Keys to extract from product data
        keys_to_check = ['code','product_name', 'brands', 'packaging', 'manufacturing_places', 
                         'image_front_url', 'packaging_materials_tags', 'packaging_hierarchy', 'origins_tags', 'ecoscore_data' ]
        for key in keys_to_check:
            value = product.get(key, 'N/A')  # Provide a default value if key doesn't exist
            if value == '':
                value = 'N/A'
            if key == 'origins_tags' and value != 'N/A':
                origins = []
                for origin in value:
                    if origin.find("uxemb") > -1:
                        clean_origin = "Luxembourg"
                    else:
                        clean_origin = origin
                        if clean_origin.find(":") > -1:
                            clean_origin = origin[3:]
                        if clean_origin.find(",") > -1:
                            clean_origin = clean_origin.split(",")[0]
                        if clean_origin.capitalize() in communes:
                            clean_origin = "Luxembourg"
                    origins.append(clean_origin)
                data[key] = origins
            else:
                data[key] = value
        manufacturing_place = product.get('manufacturing_places', 'N/A').lower()
        if manufacturing_place.find('luxemb') > -1 \
            or manufacturing_place.find('belg') > -1 \
                or manufacturing_place.find('fran') > -1 \
                    or manufacturing_place.find('germ') > -1 \
                        or manufacturing_place.find('deut') > -1\
                            or manufacturing_place.find('allem') > -1:
            data['origins_tags'].append(manufacturing_place)

        return data
    except requests.exceptions.RequestException as e:
        print(f"Failed to fetch data for barcode {barcode}: {e}")
        print("")
        return None

# Function to unify packaging materials
def search_keywords(packaging, keyword_list, db: Session, save_materials: bool):
    """
    Unifies packaging materials by replacing keywords with their replacements.

    Args:
        packaging (str or list): The original packaging description as a string or a list of strings.
        keyword_list (dict): Dictionary mapping keywords to replacements.

    Returns:
        list: The list of unified packaging materials.
    """
    package_materials = set()  # Using a set to store unique materials
    
    # Convert packaging to string if it's a list
    if isinstance(packaging, list):
        packaging = ", ".join(packaging)
        
    for keyword, replacement in keyword_list.items():
        if keyword.lower() in packaging.lower():
            package_materials.add(replacement)  # Add the replacement material to the set
    
    if package_materials:
        return sorted(package_materials)
    else:
        if save_materials:
            create_new_material(packaging, db)
        return ['N/A']

#def search_keywords2(packaging, model, db : Session = Depends(get_db)):
#    """
#    Unifies packaging materials by replacing keywords with their replacements.
#
#    Args:
#        packaging (str or list): The original packaging description as a string or a list of strings.
#        model (Base): The SQLAlchemy model containing keyword and replacement attributes.
#        db (Session): The SQLAlchemy session.
#
#    Returns:
#        list: The list of unified packaging materials.
#    """
#    package_materials = set()  # Using a set to store unique materials
#    
#    # Convert packaging to string if it's a list
#    if isinstance(packaging, list):
#        packaging = ", ".join(packaging)
#    
#    # Define keyword_attribute and replacement_attribute inside the function
#    keyword_attribute = "raw_name"
#    replacement_attribute = "name"
#        
#    for instance in db.query(model).all():
#        if getattr(instance, keyword_attribute).lower() in packaging.lower():
#            package_materials.add(getattr(instance, replacement_attribute))  # Add the replacement material to the set
#    
#    return sorted(package_materials) if package_materials else ['N/A']

# Function for later use to look for keywords in "packaging_hierarchy" LIST when elements not found in "packaging"

# Function to count the number of materials in packaging
def count_keywords(packaging,keyword_list, db: Session):
    """
    Counts the number of unique materials in the packaging.

    Args:
        packaging (list): The list of unified packaging materials.

    Returns:
        int: The number of unique materials.
    """
    # Count the materials and subtract 1 if 'N/A' is present
    elements = search_keywords(packaging,keyword_list, db, save_materials=False)
    count = len(elements)
    if 'N/A' in packaging:
        count -= 1
    return count

def extract_ecoscore(data, material_keyword_replacements, shape_keyword_replacements, db: Session):
    # Improvement necessary: if data['ecoscore_data']['adjustments']['packaging']['packagings'] doesn't exist
    packaging_list = []
    try:
        for packaging in data['ecoscore_data']['adjustments']['packaging']['packagings']:
            packaging_dict = {
                'material': packaging['material'],
                'shape': packaging['shape']
            }
            # Search and replace keywords for material and shape
            if packaging_dict['material'] == "en:unknown":
                packaging_dict['material'] = ["unknown"]
            else:
                packaging_dict['material'] = search_keywords(packaging_dict['material'], material_keyword_replacements, db, save_materials=True)

            if packaging_dict['shape'] == "en:unknown":
                packaging_dict['shape'] = ["element"]
            elif search_keywords(packaging_dict['shape'], shape_keyword_replacements, db, save_materials=False) == ['N/A']:
                if packaging_dict['shape'].find(":") > -1:
                    packaging_dict['shape'] = [packaging_dict['shape'][3:].lower()]
                else:
                    packaging_dict['shape'] = [packaging_dict['shape'].lower()]
            else:
                packaging_dict['shape'] = search_keywords(packaging_dict['shape'], shape_keyword_replacements, db, save_materials=False)
            
            # Convert material and shape lists to strings
            packaging_dict['material'] = packaging_dict['material'][0] if packaging_dict['material'] else 'unknown'
            packaging_dict['shape'] = packaging_dict['shape'][0] if packaging_dict['shape'] else "element"
            
            packaging_list.append(packaging_dict)
        return packaging_list
    except:
        return [{
            'material': 'unknown',
            'shape': 'unknown'
        }]

# Function to process product data

def process_product_data(product_data, db: Session):
    """
    Processes product data by unifying packaging materials,
    counting materials, and identifying package elements.

    Args:
        product_data (dict): The product data dictionary.

    Returns:
        dict: Processed product data.
    """
    if product_data:
        # Replace keywords in packaging and count materials
        
        packaging = product_data.get('packaging', 'N/A')
        packaging2 = product_data.get('packaging_hierarchy', 'N/A')
        
        # Create material equivalent list
        material_keyword_replacements = list_materials(db)
        
        materials1 = search_keywords(packaging, material_keyword_replacements, db, save_materials=False)
       # product_data['Package_materials1'] = materials1
        
        # Count number of materials
        num_materials1 = count_keywords(packaging, material_keyword_replacements, db)
       # product_data['Number_of_materials1'] = num_materials1
        
        # Identify package elements
        elements1 = search_keywords(packaging, element_keyword_replacements, db, save_materials=False)
        #product_data['Package_elements1'] = elements1
        
        # Count number of elements
        num_elements1 = count_keywords(packaging, element_keyword_replacements, db)
        #product_data['Number_of_elements1'] = num_elements1
        
         ## Identify package materials2
        materials2 = search_keywords(packaging2, material_keyword_replacements, db, save_materials=False)
        #product_data['Package_materials2'] = materials2
        
        # Count number of materials2
        num_materials2 = count_keywords(packaging2, material_keyword_replacements, db)
       # product_data['Number_of_materials2'] = num_materials2
        
        ## Identify package elements2
        elements2 = search_keywords(packaging2, element_keyword_replacements, db, save_materials=False)
        #product_data['Package_elements2'] = elements2
        
        # Count number of elements2
        num_elements2 = count_keywords(packaging2, element_keyword_replacements, db)
        #product_data['Number_of_elements2'] = num_elements2
        
        product_data['Package_ecoscore'] = extract_ecoscore(product_data, material_keyword_replacements, element_keyword_replacements, db)
        
        material_count = sum(1 for package in product_data['Package_ecoscore'] if package['material'] != 'N/A')
        shape_count = sum(1 for package in product_data['Package_ecoscore'] if package['shape'] != 'N/A')

        product_data['Package_ecoscore_number'] = {"material": material_count, "element": shape_count}

        
        product_data.pop('ecoscore_data', 'test')
        
        # Compare counts and update product data accordingly
        if num_materials1 + num_elements1 >= num_materials2 + num_elements2:
            product_data['Package_materials'] = materials1
            product_data['Number_of_materials'] = num_materials1
            product_data['Package_elements'] = elements1
            product_data['Number_of_elements'] = num_elements1
        else:
            product_data['Package_materials'] = materials2
            product_data['Number_of_materials'] = num_materials2
            product_data['Package_elements'] = elements2
            product_data['Number_of_elements'] = num_elements2
            
        return product_data
    return None