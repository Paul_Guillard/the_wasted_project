from fastapi import Depends, HTTPException, status
from jose import ExpiredSignatureError, JWTError, jwt
from API.v1.schemas import Token
from fastapi.security import OAuth2PasswordBearer
from datetime import datetime, timedelta, UTC
from API.v1.config import settings

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/login")

# Secret key used by JWT to sign the token - must remain secret
# Generated with openssl rand -hex 32
SERVER_KEY = settings.server_key
ALGORITHM = settings.algorithm
EXPIRATION_MINUTES = settings.expiration_minutes


def generate_token(user_id: int, role: str, commune: str) -> str:
    # User id will be stored in the JWT payload
    payload = {
        "user_id": user_id ,
        "role": role,
        "commune": commune,
        "exp": datetime.now(UTC) + timedelta(minutes=EXPIRATION_MINUTES)
    }
    encoded_jwt = jwt.encode(payload, SERVER_KEY, ALGORITHM)
    return Token(access_token=encoded_jwt, token_type="bearer", role=role)


def decode_token(provided_token: str = Depends(oauth2_scheme)) -> int:
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, [ALGORITHM])
        decoded_id = payload['user_id']
        decoded_role = payload['role']
        decoded_commune = payload['commune']
    except ExpiredSignatureError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, 
            detail="Your token has expired and needs to be created again.",
            headers={"WWW-Authenticate": "bearer"}
        )
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, 
            detail="Could not validate your credentials.",
            headers={"WWW-Authenticate": "bearer"}
        )
    return {
        "id": decoded_id, 
        "role": decoded_role, 
        "commune": decoded_commune
    }


def decode_user_with_role(role):
    def get_user_and_validate(user=Depends(decode_token)):
        if user['role'] == 'admin':
            # Admin has all rights, always ok
            return user
        elif not user['role'] == role:
            # Verify role allowance if not admin
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="You are not allowed to perform this operation.")

        return user

    return get_user_and_validate
