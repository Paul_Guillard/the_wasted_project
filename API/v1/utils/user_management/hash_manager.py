# from passlib.context import CryptContext
import bcrypt

def hash_pass(password: str) -> str:
    salt = bcrypt.gensalt()
    bytes = password.encode('utf-8') 
    hashed_pwd = bcrypt.hashpw(bytes, salt=salt)
    string_password = hashed_pwd.decode('utf-8')
    return string_password

def verify_password(plain_password: str, hash_password: str) -> bool:
    bytes = plain_password.encode('utf-8') 
    hashed = hash_password.encode('utf-8')
    return bcrypt.checkpw(password=bytes, hashed_password=hashed)