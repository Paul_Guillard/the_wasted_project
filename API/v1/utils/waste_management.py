from typing import List
from ..config import settings
from . import external_api_management
from sqlalchemy.orm import Session
from ..models import Product, Material, LinkMaterialProduct


"""
Helper function which will coordinate the actions when a barcode is input by the user
Actions performed: 
    1. Fetch data from the external API about the specific product
    2. If data is found, create the product in the database if it doesn't exist yet
    3. Evaluate the materials list based on the packaging
    4. Link the product to the corresponding materials in the database

Args: barcode -> str

Returns: Product object with all the required data for the selected product
product_data = {
    "id": int,
    "code": str,
    "product_name": str,
    brands: str,
    package_materials: list[str],
    package_elements: list[str],
    manufacturing_places: str,
    image_front_url: str,
    origins: list,
    instructions: {
        'status': 'ok', 'nok' or 'missing',
        'error': str (error details),
        'material_1': {
            "container": str - material container name,
            "shapes": str - shapes separated by ',',
            "material_instructions": str - specific instructions based on material
        },
        'material_2': {
            "container": str - material container name,
            "shapes": str - shapes separated by ',',
            "material_instructions": str - specific instructions based on material
        },
        ...
    },
    related_materials: list[
        {
            "shapes": "Lid,spoon",
            "material": {
                "raw_name": str,
                "name": str,
                "container_id": int,
                "instructions": str,
                "validated": bool,
                "id": int,
                "container": {
                    container details
                }
            }
        },
        ...
    ]
}
"""
def process_barcode(code: str, db: Session) -> dict:
    # Fetch product data from the external API
    product_data = external_api_management.get_data(code)
    if product_data and product_data['code'] != 'N/A':
        # Case product was found in the external API
        # Create new product if does not exist
        product = db.query(Product).filter(Product.code == code).first()
        if not product:
            new_product = Product(code=code, product_name=product_data['product_name'], image_front_url=product_data['image_front_url'])
            db.add(new_product)
            db.commit()
            db.refresh(new_product)
            product = new_product
        elif product.image_front_url == None:
            db.query(Product).filter(Product.code == code).update({
                "code": code,
                "product_name": product_data['product_name'],
                "image_front_url": product_data['image_front_url']
            })
            db.commit()
        
        # Compute materials list
        product_infos = external_api_management.process_product_data(product_data, db)
        
        # Recreate the list by joining shapes together when material is the same
        # Index to be checked when utils function is updated
        packages = product_infos['Package_ecoscore']
        consolidated_list = []
        for package in packages:
            already_in_list = False
            for consolidated_element in consolidated_list:
                if consolidated_element['material'] == package['material']:
                    already_in_list = True
                    # 'shape' index to be checked when utils function is updated
                    consolidated_element['shape'] += ',' + package['shape']
            if not already_in_list:
                consolidated_list.append(package)

        # Clean string from 'shapes' value at the end of the operation
        for consolidated_element in consolidated_list:
            consolidated_element['shape'] = consolidated_element['shape'].strip(",")

        materials_ids = []
        for package in consolidated_list:
            existing_material = db.query(Material).filter(Material.name == package['material']).first()

            if existing_material:
                materials_ids.append({"id": existing_material.id, "shapes": package['shape']})

        # Link materials to product from IDs
        try:
            for material_id in materials_ids:
                # material = db.query(Material).get(material_id['id'])
                # Check if link between material and product already exists.
                # If not, create the link, with the shapes as an extra parameter
                # Future improvement: if already exists, update shapes
                if not db.query(LinkMaterialProduct).filter(LinkMaterialProduct.material_id == material_id['id']).filter(LinkMaterialProduct.product_id == product.id).first():
                    new_material = LinkMaterialProduct(material_id=material_id['id'], product_id=product.id, shapes=material_id['shapes'])

                    db.add(new_material) #send the query
                    db.commit() #save staged changes to the DB
                    db.refresh(new_material)

            db.commit()
        except Exception as e:
            # In case of issue, just cancel the linking, but no code interruption
            db.rollback()

        # Add relevant data from the external API
        product.brands = product_data['brands']
        product.package_materials = product_infos['Package_materials']
        product.package_elements = product_infos['Package_elements']
        product.manufacturing_places = product_data['manufacturing_places']
        product.image_front_url = product.image_front_url # product_data['image_front_url']
        product.origins = product_data['origins_tags']

        # Add instructions
        product.instructions = list_instructions(product)

        # Return the final information   
        return product
    else:
        # Case the external API does not find the product (not found or not available)
        corresponding_product = db.query(Product).filter(Product.code == code).first()
        if corresponding_product:
            # Recovery mode: only the data from the database is used
            corresponding_product.brands = "Not available"
            corresponding_product.package_materials = ["Not available"]
            corresponding_product.package_elements = ["Not available"]
            corresponding_product.manufacturing_places = "Not available"
            # corresponding_product.image_front_url = None
            corresponding_product.origins = []
            corresponding_product.instructions = list_instructions(corresponding_product)
            return corresponding_product
        else:
            # Product doesn't exist in the DB and is not found in the external API
            return None


"""
Algorithm that computes waste instructions based on the input barcode.
Requires the barcode to be in the database already.
Instructions will be taken from the database, based on the materials of the product

Args: barcode (str) - Input by the user

Output: List[str] - List of instructions
"""
def list_instructions(product: Product) -> List[str]:
    try:
        # Collect product materials
        materials = product.related_materials
        
        # Adapt the messages based on the number of materials available
        if len(materials) == 0:
            instructions = {
                'status': 'missing',
                'error': ''
            }
        elif len(materials) == 1:
            if not materials[0].material.container_id:
                container = 'unknown'
            else:
                container = materials[0].material.container.name_disp
            instructions = {
                'status': 'ok',
                'error': '',
                materials[0].material.name: {
                    "container": container,
                    "shapes": materials[0].shapes,
                    "material_instructions": materials[0].material.instructions
                }
            }
        else:
            # In case of several materials, instructions will be given for each material
            # Material duplicates are removed to avoid displaying several times the same instruction
            instructions = {
                'status': 'ok',
                'error': ''
            }
            used_materials = []
            for material in materials:
                if not material.material.container_id:
                    container = 'unknown'
                else:
                    container = material.material.container.name_disp
                if material.material.name not in used_materials:
                    instructions[material.material.name] = {
                        "container": container,
                        "shapes": material.shapes,
                        "material_instructions": material.material.instructions
                    }
                    used_materials.append(material.material.name)

    except Exception as e:
        # Display an error message in case data could not be fetched
        instructions = {
            'status': 'nok',
            'error': f"Error: {e}"
        }
    
    return instructions
