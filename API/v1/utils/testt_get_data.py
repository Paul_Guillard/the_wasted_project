from external_api_management import get_data, process_product_data, search_keywords, material_keyword_replacements, element_keyword_replacements

# Function to test fetching data

def testing_fetching(barcodes):
    """
    Test fetching product data for a list of barcodes.
    """
    for barcode in barcodes:
        product_data = get_data(barcode)
        if product_data:
            processed_product_data = process_product_data(product_data)
            if processed_product_data:
                print("Data:")
                for key, value in processed_product_data.items():
                    print(f"{key.capitalize()}: {value}")
                print()

# List of barcodes to fetch data for
barcodes = [8718096160714, 5450168511156, 7612100018477, 87157321, 4008258154236, 5410046000141, 5060108450546, 4018077006425, 
            20697075, 5450148000014, 27043189, 8002974025708, 23287235, 4002309012933, 3297362470009, 40081427, 
            5410228253556, 3563490016908,5901044003581, 5904194002107, 5907544131441, 5904378643270 ]
barcodes1 = [5450168511156, 7612100018477, 87157321, 4008258154236, 5410046000141, 5060108450546, 4018077006425, 5400119580489, 5450168561090] 
barcodes2 = [20697075, 5450148000014, 27043189, 8002974025708, 23287235, 4002309012933, 3297362470009, 40081427, 5410228253556, 3563490016908]

barcodesPL = [5901044003581, 5904194002107, 5907544131441, 5904378643270]

def extract_ecoscore_package(product_data):
    packaging_list = []
    for packaging in product_data['ecoscore_data']['adjustments']['packaging']['packagings']:
        packaging_dict = {'material': packaging['material'], 'shape': packaging['shape']}
        packaging_list.append(packaging_dict)
    return packaging_list


def extract_packaging_info(data, material_keyword_replacements, shape_keyword_replacements):
    packaging_list = []
    for packaging in data['ecoscore_data']['adjustments']['packaging']['packagings']:
        packaging_dict = {
            'material': packaging['material'],
            'shape': packaging['shape']
        }
        # Search and replace keywords for material and shape
        packaging_dict['material'] = search_keywords(packaging_dict['material'], material_keyword_replacements)
        packaging_dict['shape'] = search_keywords(packaging_dict['shape'], shape_keyword_replacements)
        
        # Convert material and shape lists to strings
        packaging_dict['material'] = packaging_dict['material'][0] if packaging_dict['material'] else 'N/A'
        packaging_dict['shape'] = packaging_dict['shape'][0] if packaging_dict['shape'] else 'N/A'
        
        packaging_list.append(packaging_dict)
    return packaging_list



# Call the function to test fetching
print("")
print("wtf")
print("")
testing_fetching(["7612100018477"])

