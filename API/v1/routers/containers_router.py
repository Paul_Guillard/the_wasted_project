from typing import List
from fastapi import APIRouter, Depends, HTTPException, status, Response
from sqlalchemy.orm import Session
from fastapi import Depends
from ..database import get_db
import API.v1.models as models
from ..schemas import Container_response, Container_request, Material_response, Material_request
from typing import List
from ..utils.user_management import jwt_manager


router = APIRouter(prefix="/containers", tags=["Containers"])



@router.get("/", response_model=List[Container_response])
def get_containers(db: Session = Depends(get_db)):
    containers = db.query(models.Container).all()

    return containers


@router.get("/{id}", response_model=Container_response)
def get_container_by_id(id: int, db: Session = Depends(get_db)):
    container = db.query(models.Container).filter(models.Container.id == id).first()

    if not container:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    return container


@router.post("/", response_model=Container_response, status_code=status.HTTP_201_CREATED)
def create_container(container_body: Container_request, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    new_container = models.Container(**container_body.model_dump())
    db.add(new_container) #send the query
    db.commit() #save staged changes to the DB
    db.refresh(new_container) 

    if not new_container:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    return new_container

@router.put("/{id}", response_model=Container_response)
def update_container(container_body: Container_request, id: int, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    updated_container = db.query(models.Container).filter(models.Container.id == id)

    if not updated_container:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    updated_container.update(container_body.model_dump())
    db.commit()
    
    return updated_container.first()

@router.delete("/{id}", response_model=Container_response)
def delete_container(id: int, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    container_to_be_deleted = db.query(models.Container).filter(models.Container.id == id)

    if not container_to_be_deleted:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    #fetch materials with contaner_to_be_deleted id
    materials_to_update = db.query(models.Material).filter(models.Material.container_id == id).all()
    
    #loop through esch material
    for material in materials_to_update:
        #set container_id to null (None)       
        material.container_id = None
        db.add(material)
        
    db.commit()
    
    container_to_be_deleted.delete()
    db.commit()
    
    return Response(status_code=status.HTTP_204_NO_CONTENT)




