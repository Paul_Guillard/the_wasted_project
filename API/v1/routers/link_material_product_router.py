from fastapi import APIRouter, Depends, HTTPException, status, Response
from psycopg2 import IntegrityError
from sqlalchemy.orm import Session
from fastapi import Depends
from ..database import get_db
from ..models import Product, Material
from ..schemas import Product_response, Product_request, LinkMaterialProduct_request
from ..utils.external_api_management import get_data, process_product_data

router = APIRouter(prefix="/linkmp", tags=["Linkmp"])

@router.post("/", status_code=status.HTTP_201_CREATED)
def create_link_material_product(product_body: LinkMaterialProduct_request, db: Session = Depends(get_db)):
    try:
        product = db.query(Product).get(product_body.product_id)
        if product is None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Product not found")
        
        for material_id in product_body.materials_ids:
            material = db.query(Material).get(material_id)
            if material is None:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Material with ID {material_id} not found")
            
            product.related_materials.append(material)

        db.commit()
        return {"message": "Linking successful"}
    except IntegrityError:
        db.rollback()
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="IntegrityError occurred while linking materials")
    except Exception as e:
        db.rollback()
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))
