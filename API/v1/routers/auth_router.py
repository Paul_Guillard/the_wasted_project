from fastapi import APIRouter, Depends, HTTPException, status, Response
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
import API.v1.models as models
from ..database import get_db
from ..schemas import Token, User_credentials, User_request
from ..utils.user_management import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from ..config import settings

router = APIRouter(prefix="/auth", tags=["Authentication"])

wrongCredentialsException = HTTPException(
                                status_code=status.HTTP_401_UNAUTHORIZED, 
                                detail="Could not validate your credentials.",
                                headers={"WWW-Authenticate": "bearer"}
                            )

# POST credentials
# Authentication with OAuth, for the docs
# If not using Oauth, use user_credentials: User_credentials and filter with user_credentials.email
@router.post('/login', response_model=Token)
def auth_user(user_credentials: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    # Query database to find the user
    corresponding_user = db.query(models.User).filter(models.User.email == user_credentials.username).first()
    if not corresponding_user:
        raise wrongCredentialsException
    
    # Check the password
    pwd_check = hash_manager.verify_password(user_credentials.password, corresponding_user.password)
    if not pwd_check:
        raise wrongCredentialsException
    
    # Generate new token for the authenticated user
    token = jwt_manager.generate_token(user_id=corresponding_user.id, role=corresponding_user.role, commune=corresponding_user.commune)
    token.role = corresponding_user.role
    return token


@router.post('/register', response_model=Token, status_code=status.HTTP_201_CREATED)
def create_user(user_body: User_request, db: Session = Depends(get_db)):
    try:
        new_user = models.User(**user_body.model_dump())
        new_user.password = hash_manager.hash_pass(user_body.password)
        if user_body.email == settings.admin_email:
            new_user.role = 'admin'
        db.add(new_user)
        db.commit()
        db.refresh(new_user)
    except IntegrityError:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="This email is already used!")
    
    if not new_user:
        raise wrongCredentialsException
    
    # Generate new token for the authenticated user
    token = jwt_manager.generate_token(user_id=new_user.id, role=new_user.role, commune=new_user.commune)
    token.role = new_user.role
    return token
