from typing import List
from fastapi import APIRouter, Depends, HTTPException, status, Response
from sqlalchemy.orm import Session
from fastapi import Depends
from ..database import get_db
import API.v1.models as models
from ..schemas import ImprovementRequest, ImprovementResponse
from typing import List
from ..utils.user_management import jwt_manager


router = APIRouter(prefix="/improvements", tags=["Improvements"])



@router.get("/", response_model=List[ImprovementResponse])
def get_improvements(db: Session = Depends(get_db)):
    improvements = db.query(models.Improvement).all()

    return improvements


@router.get("/{id}", response_model=ImprovementResponse)
def get_improvement_by_id(id: int, db: Session = Depends(get_db)):
    improvement = db.query(models.Improvement).filter(models.Improvement.id == id).first()

    if not improvement:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    return improvement


@router.post("/", response_model=ImprovementResponse, status_code=status.HTTP_201_CREATED)
def create_improvement(improvement_body: ImprovementRequest, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('guest'))):
    new_improvement = models.Improvement(**improvement_body.model_dump())
    new_improvement.user_id = user['id'] # jwt_manager.decode_token(improvement_body.user_token)['id']
    db.add(new_improvement) #send the query
    db.commit() #save staged changes to the DB
    db.refresh(new_improvement) 

    if not new_improvement:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    return new_improvement

# @router.put("/{id}", response_model=Container_response)
# def update_container(container_body: Container_request, id: int, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
#     updated_container = db.query(models.Container).filter(models.Container.id == id)

#     if not updated_container:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
#     updated_container.update(container_body.model_dump())
#     db.commit()
    
#     return updated_container.first()

@router.delete("/{id}")
def delete_improvement(id: int, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    improvement_to_be_deleted = db.query(models.Improvement).filter(models.Improvement.id == id)

    if not improvement_to_be_deleted.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    improvement_to_be_deleted.delete()
    db.commit()
    
    return Response(status_code=status.HTTP_204_NO_CONTENT)




