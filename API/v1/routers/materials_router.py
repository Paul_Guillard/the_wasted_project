from typing import List
from fastapi import APIRouter, Depends, HTTPException, status, Response
from sqlalchemy.orm import Session
from fastapi import Depends
from ..database import get_db
import API.v1.models as models
from ..schemas import Material_request, Material_response
from typing import List
from ..utils.user_management import jwt_manager


router = APIRouter(prefix="/materials", tags=["Materials"])



@router.get("/", response_model=List[Material_response])
def get_all_materials(db: Session = Depends(get_db)):
    all_materials = db.query(models.Material).all()

    return all_materials


@router.get("/{id}", response_model=Material_response)
def get_material_by_id(id: int, db: Session = Depends(get_db)):
    material = db.query(models.Material).filter(models.Material.id == id).first()

    if not material:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    return material

@router.post("/", response_model=Material_response, status_code=status.HTTP_201_CREATED)
def create_material(material_body: Material_request, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    new_material = models.Material(**material_body.model_dump())
    db.add(new_material) #send the query
    db.commit() #save staged changes to the DB
    db.refresh(new_material) 

    if not new_material:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    return new_material

@router.delete("/{id}", response_model=Material_response)
def delete_material(id: int, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    deleted_material = db.query(models.Material).filter(models.Material.id == id)

    if not deleted_material:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    deleted_material_element = deleted_material.first()
    for product in deleted_material_element.related_products:
        deleted_material_element.related_products.remove(product)
        db.add(product)
    db.commit()

    deleted_material.delete() #delete the material
    db.commit() #save changes to DB
    
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put("/{id}", response_model=Material_response)
def update_material(material_body: Material_request, id: int, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    updated_material = db.query(models.Material).filter(models.Material.id == id)

    if not updated_material:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    updated_material.update(material_body.model_dump()) #update the material
    db.commit() #save changes to DB
    
    return updated_material.first()
