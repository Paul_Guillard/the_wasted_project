from typing import Optional
from fastapi import APIRouter, Depends, HTTPException, status, Response
from sqlalchemy.orm import Session
from fastapi import Depends
from ..database import get_db
from ..models import Product, LinkMaterialProduct
from ..schemas import Product_response, Product_request
from ..utils.waste_management import process_barcode
from sqlalchemy.exc import IntegrityError
from typing import List
from ..utils.user_management import jwt_manager

router = APIRouter(prefix="/products", tags=["Products"])

@router.get("/{barcode}", response_model=Optional[Product_response])
def get_product_by_barcode(barcode: str, db: Session = Depends(get_db)):
    if not barcode:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="The barcode format used is not correct")
    
    # Build the database based on the user input 
    product = process_barcode(barcode.strip(), db)

    if not product:
         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Could not find data related to this product")
    return product

@router.post("/", response_model=Product_response, status_code=status.HTTP_201_CREATED)
def create_product(product_body : Product_request, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    try:
        new_product = Product(**product_body.model_dump()) #create new product
        db.add(new_product) #send the query
        db.commit() #Save the staged changes to the DB
        db.refresh(new_product) # to replace "RETURNING *"
    except IntegrityError as ie:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Non-existent or invalid foreign key : {ie}")   
    return new_product

@router.get("/", response_model=List[Product_response])
def get_all_products(db: Session = Depends(get_db)):
    all_products = db.query(Product).all()
    
    return all_products

@router.put("/{barcode}", response_model=Optional[Product_response])
def update_product(product_body : Product_request, barcode: str, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    updated_product = db.query(Product).filter(Product.code == barcode)

    if not updated_product:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="This id was not found in our records...")
    
    updated_product.update(product_body.model_dump()) #update the product
    db.commit() #save changes to DB
    
    return updated_product.first()

@router.delete("/{barcode}", response_model=Product_response)
def delete_product(barcode: str, db: Session = Depends(get_db), user: dict = Depends(jwt_manager.decode_user_with_role('admin'))):
    query_deleted_product: str = db.query(Product).filter(Product.code == barcode)
    deleted_product = query_deleted_product.first()
    
    if not deleted_product:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="The ID was not found in our records...")
    
    deleted_product_link = db.query(LinkMaterialProduct).filter(LinkMaterialProduct.product_id == deleted_product.id)

    try:
        deleted_product_link.delete()
    except:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="An unexpected [issue occurred while deleting the product.")

    db.delete(deleted_product) #delete the product
    db.commit() #save changes to DB
    
    return Response(status_code=status.HTTP_204_NO_CONTENT)