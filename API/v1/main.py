"""
The main file should be the core of the back-end code
"""

from .database import database_engine
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .routers import products_router, containers_router, materials_router, auth_router, improvements_router
import API.v1.models as models
from .config import settings


# Create the tables if they don't exist yet
if settings.reload_db:
    models.Base.metadata.drop_all(bind=database_engine)
models.Base.metadata.create_all(bind=database_engine)

# Here read JSON file from data folder and write content in database if table is empty
# Can be done for containers, materials


# Run the server
app = FastAPI()

# Add a CORS middleware to filter/authorize the API requests based on source, action, ...
app.add_middleware(
    CORSMiddleware,
    allow_origins = ['*'],
    allow_credentials =  True,
    allow_methods = ['*'],
    allow_headers = ['*']
)

app.include_router(products_router.router)
app.include_router(containers_router.router)
app.include_router(materials_router.router)
app.include_router(auth_router.router)
app.include_router(improvements_router.router)
