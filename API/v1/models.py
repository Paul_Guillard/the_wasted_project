from typing import List, Optional
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ARRAY, Column, Integer, PrimaryKeyConstraint, String, Boolean, ForeignKey, DateTime, Table, Time
from sqlalchemy.sql.sqltypes import TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.orm import Mapped, mapped_column, relationship, DeclarativeBase
# from sqlalchemy.ext.associationproxy import association_proxy

# Base is a class for all created models
class Base(DeclarativeBase):
    pass


class LinkMaterialProduct(Base):
    __tablename__ = "link_material_product" # name in postgres
    material_id: Mapped[int] = mapped_column(ForeignKey("materials.id"), nullable=True, primary_key=True)
    material: Mapped["Material"] = relationship(back_populates="related_products")
    product_id: Mapped[int] = mapped_column(ForeignKey("products.id"), nullable=True, primary_key=True)
    product: Mapped["Product"] = relationship(back_populates="related_materials")
    shapes: Mapped[str] = Column(String, nullable=False, server_default="element")


class Product(Base):
    __tablename__ = "products" # name in postgres
    # id = Column(Integer, primary_key=True, nullable=False)
    id: Mapped[int] = mapped_column(primary_key=True)
    code = Column(String, nullable=False, unique=True) # Updated upon user search
    product_name = Column(String, nullable=False) # Updated after external API response
    created_at = Column(TIMESTAMP(timezone=True), server_default=text("now()"), nullable=False) # Updated automatically upon creation
    image_front_url = Column(String,nullable=True) # Updated after external API response
    
    # Many to many relationships 
    related_materials: Mapped[List["LinkMaterialProduct"]] = relationship(back_populates="product")


class Material(Base):
    __tablename__ = "materials" # name in postgres
    id: Mapped[int] = mapped_column(primary_key=True)
    raw_name = Column(String, nullable=False) # Name in the origin language as found in the API
    name = Column(String, nullable=False) # To be pre-filled by admin. Add automatically if data is missing?
    # container_id = Column(Integer, ForeignKey("containers.id", ondelete="CASCADE"), nullable=False) # To be pre-filled by admin
    container_id: Mapped[int] = mapped_column(ForeignKey("containers.id"), nullable=True)
    container: Mapped["Container"] = relationship(back_populates="materials")
    instructions = Column(String, nullable=True) #instructions to dispose each material
    validated = Column(Boolean, server_default=text("FALSE"), nullable=False)

    # Many to many relationship
    related_products: Mapped[List["LinkMaterialProduct"]] = relationship(back_populates="material")


class Container(Base):
    __tablename__ = "containers" # name in postgres
    id: Mapped[int] = mapped_column(primary_key=True)
    name = Column(String, nullable=False) # 
    accepted = Column(ARRAY(String), nullable=False) #list
    not_accepted = Column(ARRAY(String), nullable=False) #list
    materials: Mapped[List["Material"]] = relationship(back_populates="container")
    # related_collections = relationship("LinkCollectionContainer", back_populates="related_containers")
    name_disp = Column(String, nullable=True) # to display the name to the final user


class Collection(Base):
    __tablename__ = "collection_days" # name in postgres
    id = Column(Integer, primary_key=True, nullable=False)
    location = Column(String, nullable=False)
    date = Column(DateTime(timezone=True), nullable=False) #list
    # related_containers = relationship("LinkCollectionContainer", back_populates="related_collections")


class User(Base):
    __tablename__ = "users" # name in Postgres
    id: Mapped[int] = mapped_column(primary_key=True)
    first_name = Column(String, nullable=False) # To be filled by user
    last_name = Column(String, nullable=False) # To be filled by user
    email = Column(String, nullable=False, unique=True) # To be filled by user
    commune = Column(String, nullable=True) # To be filled by user, based on select list
    password = Column(String, nullable=False) # To be filled by user and hashed
    role = Column(String, nullable=False, server_default="guest") # User role: guest or admin
    created_at = Column(TIMESTAMP(timezone=True), server_default=text('now()'), nullable=False) # Automatically updated upon creation
    improvements: Mapped[List["Improvement"]] = relationship(back_populates="user")


# Improvements on products suggested by the users
class Improvement(Base):
    __tablename__ = "improvements" # name in postgres
    # id = Column(Integer, primary_key=True, nullable=False)
    id: Mapped[int] = mapped_column(primary_key=True)
    created_at = Column(TIMESTAMP(timezone=True), server_default=text("now()"), nullable=False)
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"), nullable=True)
    user: Mapped["User"] = relationship(back_populates="improvements")
    code = Column(String, nullable=False)
    product_name = Column(String, nullable=True)
    brand = Column(String, nullable=True)
    image_front_url = Column(String,nullable=True)
    origins = Column(String, nullable=True)
    element_1 = Column(String, nullable=True)
    material_1 = Column(String, nullable=True)
    element_2 = Column(String, nullable=True)
    material_2 = Column(String, nullable=True)
    element_3 = Column(String, nullable=True)
    material_3 = Column(String, nullable=True)
    element_4 = Column(String, nullable=True)
    material_4 = Column(String, nullable=True)
    element_5 = Column(String, nullable=True)
    material_5 = Column(String, nullable=True)

# class Shop(Base):
#     __tablename__ = "shops" # name in Postgres
#     id = Column(Integer, primary_key=True, nullable=False)
#     shop_name = Column(String, nullable=False, unique=False)
#     location = Column(String, nullable=False, unique=False)
#     open_hour = Column(Time, nullable=False, unique=False)
#     close_hour = Column(Time, nullable=False, unique=False)
#     created_at = Column(TIMESTAMP(timezone=True), server_default=text('now()'), nullable=False)
