from typing import List, Optional, Union
from pydantic import BaseModel, EmailStr, HttpUrl
from datetime import datetime


# Token model for JWT management (stateless API)
class Token(BaseModel):
    access_token: str
    token_type: str
    role: Optional[str] # Required for Streamlit

# SPAM protect it
class User_request(BaseModel):
    first_name: str
    last_name: str
    commune: str
    email: EmailStr # str is too general (random text)
    password: str

# User credentials schema request based on User_request
class User_credentials(User_request):
    pass # pass means we want the same fields

# User pydantic schema reponse
class User_response(User_request):
    id: int
    created_at: datetime
    improvements: List["ImprovementRequest"]
    class Config: # Important for pydantic schema translation
        # orm_mode = True
        # v2 'orm_mode' has been renamed to 'from_attributes'
        from_attributes = True


class User_info(BaseModel):
    first_name: str
    last_name: str
    commune: str
    email: EmailStr # str is too general (random text)
    class Config: # Important for pydantic schema translation
        # orm_mode = True
        # v2 'orm_mode' has been renamed to 'from_attributes'
        from_attributes = True


#USER       
class Product_request(BaseModel):
    code : str
    product_name : str
    image_front_url : Optional[str]

class Product_response(Product_request):
    id : int
    related_materials: List["LinkMaterialProductResponse"]
    manufacturing_places : Optional[str] = None # Optional[List[str]]
    brands: Optional[str] = None # Optional[List[str]]
    origins: Union[Optional[str], Optional[List[str]]] = None
    instructions: Optional[dict] = None
    # users: Optional[List[User_response]]
    # packaging : str
    # packaging_materials_tags: List[str]  # List of strings
    # packaging_hierarchy: List[str]  # List of strings
    package_materials : Optional[List[str]] = None
    # number_of_materials : int
    package_elements : Optional[List[str]] = None
    # number_of_elements : int
    # recycling_instructions : List[str]
    class Config:
        from_attributes = True

class Container_request(BaseModel):
    name : str
    accepted : Optional[List[str]]
    not_accepted : Optional[List[str]]
    name_disp : Optional[str]
class Container_response(Container_request):
    id : int
    # materials: List["Material_response"]
        
    class Config: 
        from_attributes = True

class Material_request(BaseModel):
    raw_name: str
    name : str
    container_id: Optional[int] = None
    instructions : Optional[str]
    validated: bool = True
   
class Material_response(Material_request):
    id: int
    container : Optional[Container_response]
    # material_product_links: Optional[List["LinkMaterialProductResponse"]]
    class Config:
        from_attributes = True


class ImprovementRequest(BaseModel):
    code: str
    product_name: Optional[str]
    brand: Optional[str]
    image_front_url: Optional[str]
    origins: Optional[str]
    element_1: Optional[str]
    material_1: Optional[str]
    element_2: Optional[str]
    material_2: Optional[str]
    element_3: Optional[str]
    material_3: Optional[str]
    element_4: Optional[str]
    material_4: Optional[str]
    element_5: Optional[str]
    material_5: Optional[str]
    # user_token: str

class ImprovementResponse(BaseModel):
    id: int
    created_at: datetime
    code: str
    product_name: Optional[str]
    brand: Optional[str]
    image_front_url: Optional[str]
    origins: Optional[str]
    element_1: Optional[str]
    material_1: Optional[str]
    element_2: Optional[str]
    material_2: Optional[str]
    element_3: Optional[str]
    material_3: Optional[str]
    element_4: Optional[str]
    material_4: Optional[str]
    element_5: Optional[str]
    material_5: Optional[str]
    user: User_info

### LINK Material - Product
class LinkMaterialProduct_request(BaseModel):
    product_id : int
    materials_ids : List[int]
    
class LinkMaterialProduct_response(LinkMaterialProduct_request):
    related_products: Product_response
    related_materials: Material_response
    class Config:
        from_attributes = True


class LinkMaterialProductRequest(BaseModel):
    product_id : int
    material_id : int
    shapes: Optional[str]
    
class LinkMaterialProductResponse(BaseModel):
    # product: Product_response
    shapes: Optional[str]
    material: Material_response
    class Config:
        from_attributes = True


### LINK User - Product
class LinkProductUserRequest(BaseModel):
    product_id : int
    user_id : int
    
class LinkProductUserResponse(LinkProductUserRequest):
    class Config:
        from_attributes = True
